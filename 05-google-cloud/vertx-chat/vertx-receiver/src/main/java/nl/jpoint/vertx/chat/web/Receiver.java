package nl.jpoint.vertx.chat.web;

import nl.jpoint.vertx.chat.api.AbstractChatClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Receiver extends AbstractChatClient {
    private final static Logger LOG = LoggerFactory.getLogger(Receiver.class);

    public Receiver(String handle) {
        super(handle);
    }

    @Override
    public void start() throws Exception {
        getVertx().eventBus().consumer(CHANNEL, message -> {
            LOG.info("Receiver {} got message {}", handle, message.body());
        });
    }
}

