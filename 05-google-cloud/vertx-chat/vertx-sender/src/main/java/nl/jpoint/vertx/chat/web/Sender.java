package nl.jpoint.vertx.chat.web;

import io.vertx.core.eventbus.EventBus;
import nl.jpoint.vertx.chat.api.AbstractChatClient;

public class Sender extends AbstractChatClient {
    public Sender(String handle) {
        super(handle);
    }

    @Override
    public void start() throws Exception {
        EventBus eventBus = getVertx().eventBus();
        vertx.setPeriodic(2000, event -> {
            eventBus.publish(
                CHANNEL,
                "Hello from " + handle
            );
        });
    }
}
