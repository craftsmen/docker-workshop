## Maven

```
docker build -t angular-workshop 01-angular
docker run -p 8080:80 angular-workshop
```

## Spring Boot

Run with

```
mvn clean package docker:build
docker run -p 7777:8080 lunch-is-awesome
```


## The docker registry mirror
*from top-level*

```
docker run -d -p 5000:5000 --restart=always --name registry -v `pwd`/docker-registry-mirror-config.yml:/etc/docker/registry/config.yml registry:2
```
