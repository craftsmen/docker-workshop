This example depends on a running Zookeeper cluster with nodes "zk-1", "zk-2" and "zk-3".

To start this on your Google Container Engine, run the following command while being logged in with gcloud:

```
kubectl create -f https://k8s.io/docs/tutorials/stateful-application/zookeeper.yaml
```

After that, you can build and deploy the VertX sender and receiver to the Google Registry.

```
mvn clean package

docker build -t eu.gcr.io/craftsmen-docker/vertx-sender:v1 vertx-chat/vertx-sender
docker build -t eu.gcr.io/craftsmen-docker/vertx-receiver:v1 vertx-chat/vertx-receiver

gcloud docker -- push eu.gcr.io/craftsmen-docker/vertx-sender:v1
gcloud docker -- push eu.gcr.io/craftsmen-docker/vertx-receiver:v1
```

Now, you can experiment with the sender and receiver deployments:

```
kubectl apply -f vertx-chat/vertx-sender/vertx-sender.yaml
kubectl apply -f vertx-chat/vertx-receiver/vertx-receiver.yaml
```
