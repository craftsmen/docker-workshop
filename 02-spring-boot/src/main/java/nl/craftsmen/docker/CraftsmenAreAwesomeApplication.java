package nl.craftsmen.docker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
@Controller
public class CraftsmenAreAwesomeApplication {

	@RequestMapping("/")
	public String home(Model model) {
		model.addAttribute("who", System.getenv().getOrDefault("WHO", "World"));
		model.addAttribute("hostname", System.getenv().getOrDefault("HOSTNAME", "unknown"));
		return "index";
	}

	public static void main(String[] args) {
		SpringApplication.run(CraftsmenAreAwesomeApplication.class, args);
	}
}
